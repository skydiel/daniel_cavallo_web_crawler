import mock

from web_crawler.tests.base import BaseTest


class TestPreferencesView(BaseTest):
    def setup_method(self, method):
        from web_crawler import main
        from webtest import TestApp

        app = main({}, **self.settings)
        self.testapp = TestApp(app)

    def test_get_homepage(self):
        self.testapp.get('/', status=200)

    def test_get_health(self):
        self.testapp.get('/health', status=200)

    def test_get_crawl_with_no_params_error(self):
        self.testapp.get('/crawl', status=400)

    def test_get_crawl_missing_search_text_error(self):
        self.testapp.get('/crawl?hits=10', status=400)

    def test_get_crawl_invalid_hits_error(self):
        self.testapp.get('/crawl?q=some+text&hits=0', status=400)

    @mock.patch('web_crawler.api.views.CrawlerProcess')
    def test_get_crawl_success(self, crawler_process_mock):
        self.testapp.get('/crawl?q=some+text&hits=1', status=200)

        assert crawler_process_mock.call_count == 1