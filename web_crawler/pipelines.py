# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import re

from web_crawler.items import crawled_items


class CrawlerPipeline(object):

    def process_item(self, item, spider):

        # remove CDATA HTML entity
        item['content'] = re.sub(r'<!\[CDATA\[.*?\]\]>', '', item['content'])
        # remove HTML comments
        item['content'] = re.sub(r'(<!--.*?-->)', '', item['content'], flags=re.DOTALL)
        # replace new-line, tabs and carriage returns escape sequence, and consecutive whitespaces
        item['content'] = re.sub(r'(\t|\n|\r|\")', ' ', item['content'])
        item['content'] = re.sub(r'(  +)', ' ', item['content'])

        item['size'] = len(item['content'])

        crawled_items.append(dict(item))

        return item
