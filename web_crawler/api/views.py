from pyramid.view import view_config
from pyramid.httpexceptions import HTTPBadRequest
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from web_crawler.items import crawled_items


class CrawlerViews:

    def __init__(self, request):
        self.request = request
        self.settings = request.registry.settings
        self.search_provider = self.settings['default_search_provider']
        self.top_ranked_sites_count = int(self.settings['top_ranked_sites_count'])

    @view_config(route_name="home", request_method="GET", renderer='json')
    def home(self):
        import yaml
        return yaml.load(open('web_crawler/api/swagger.yaml', 'r'))

    @view_config(route_name="health", request_method="GET", renderer='string')
    def health(self):
        return 'I am felling well today. Thanks for asking.'

    @view_config(route_name="crawl", request_method="GET", renderer='json')
    def crawl(self):

        search_text = self.request.GET.get('q')
        hits = int(self.request.GET.get('hits', self.top_ranked_sites_count))

        if not (search_text and hits > 0):
            raise HTTPBadRequest('Required search text not given or hits has invalid value.')

        process = CrawlerProcess(get_project_settings())
        process.crawl(self.search_provider, search_text=search_text, sites_count=hits)
        process.start()

        return crawled_items
