# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class CrawlerItem(scrapy.Item):
    # define the fields for your item here like:
    name = scrapy.Field()
    url = scrapy.Field()
    html = scrapy.Field()
    content = scrapy.Field()
    size = scrapy.Field()
    crawled_at = scrapy.Field()


crawled_items = []
