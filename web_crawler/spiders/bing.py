from datetime import datetime
from scrapy import Spider
from scrapy.http import Request
from urllib.parse import quote_plus

from web_crawler.items import CrawlerItem


class BingSearchSpider(Spider):
    name = "bing"
    # allowed_domains = ['bing.com']
    exclude_tags = "header, footer, img, svg, iframe, script"

    def __init__(self, search_text, sites_count, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.search_text = search_text
        self.sites_count = sites_count
        self.language = kwargs.get('language', 'en')

        _search_string = quote_plus(f'{search_text} language:{self.language}')

        self. start_urls = [
            f'http://www.bing.com/search?q={_search_string}',
        ]

        self.tag_selection = f'/html/body//*[not(contains("{self.exclude_tags}", name()))]'

    def parse(self, response):
        selected_sites = response.xpath('//li[@class="b_algo"]//h2/a')
        for sel in selected_sites[:self.sites_count]:
            name = ''.join(sel.css("*::text").getall())
            url = sel.xpath('./@href').get()
            if len(url):
                yield Request(url=url,
                              callback=self.parse_item,
                              meta={'name': name})

    def parse_item(self, response):
        return CrawlerItem(name=response.meta['name'],
                           url=response.url,
                           html=response.body.decode(),
                           content=' '.join(response.xpath(self.tag_selection).css('*::text').getall()),
                           crawled_at=datetime.utcnow().isoformat())
