# WEB CRAWLER

A service responsible for crawling the best ranked sites in the default search engine.

## Installation

This project requires Python 3+ compiled with sqlite3.

_Note: if you are using a virtualenv manager like `pyenv` that compiles python on installation,
you should garantee that the `libsqlite3-dev` library is installed in your system._

## Testing

```
make test
```

### Using virtual environment

You can use whatever virtualenv manager you like. The one used here is just a suggestion.

1. First create a virtualenv
```
python3 -m venv .venv
source .venv/bin/activate
```
2. Run the installer
```
make install
```
3. Run the application
```
make run
```

### Using Docker container

Make sure you have [Docker](https://docs.docker.com/v17.12/install/) installed on your machine.

1. Build the application image
```
docker build --tag web-crawler .
```
2. Run a containerized instance of the application
```
docker run -d --rm --name web-crawler-app -p 8000:8000 -e ENVIRONMENT=development web-crawler
```

## Configuration

You can configure which search provider is the default and how many top hanked sites the crawler should return.
Currently, only the Bing search engine is implemented. Check the `.ini` files in the project's root folder 
to find the settings you should change.

## API Documentation

If you are running this service locally, on the default port, you can view thr API documentation on
http://localhost:8000/.

The document returned can be better viewed on https://editor.swagger.io/.

## Limitation

The current implementation of the crawler does not deal with pagination, which means that only the sites
listed in the first page of the search engine's results will be crawled.
