ifndef APP_ENVIRONMENT
	APP_ENVIRONMENT = development
endif

.PHONY: clean-build clean-pyc clean-test clean test init-db

clean-build:
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test:
	rm -fr .pytest_cache

clean: clean-build clean-pyc clean-test

install: clean
	pip install --upgrade pip setuptools
	pip install -e ".[test]"

run:
	gunicorn --paste $(APP_ENVIRONMENT).ini --max-requests 1

test:
	pytest web_crawler/tests
