from setuptools import setup

# List of dependencies installed via `pip install -e .`
# by virtue of the Setuptools `install_requires` value below.
requires = [
    'gunicorn',
    'pyramid',
    'pyyaml',
    'scrapy',
    'waitress',
]

# List of dependencies installed via `pip install -e ".[dev]"`
# by virtue of the Setuptools `extras_require` value in the Python
# dictionary below.
test_requires = [
    'mock',
    'pytest',
    'webtest',
]

setup(
    name='web_crawler',
    version='1.0.0',
    install_requires=requires,
    extras_require={
        'test': test_requires,
    },
    entry_points={
        'paste.app_factory': [
            'main = web_crawler:main'
        ],
    }
)
